int val, r, high_byte, low_byte;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  val = analogRead(0);
  //Serial.println(val);

  if (Serial.available() >= 1)
  {
    r = Serial.read();

    high_byte = (val & 0xFF00) >> 8;
    low_byte = val & 0x00FF;
    Serial.write(high_byte);
    Serial.write(low_byte);
  }

  delay(20);
}
