PImage img0, img1, img2;
int X=800/2;
int Y=0;
int count = 0;
int add = 6;
int miss = 0;
int[][] apple_n = new int[5][2];
int i;

//serial communication
import processing.serial.*;

Serial myserial;
int r_data, z_data, s_flag, s, high_byte, low_byte;


void setup() {
  size(800, 540);
  img0 = loadImage("Park.png");
  img1 = loadImage("Apple.png");
  img2 = loadImage("Zaru.png");
  frameRate(150);
  imageMode(CENTER);

  for(i=0;i<=1;i++){
    if(i==0){
     apple_n[i][0]=X;
     apple_n[i][1]=0;
    }else{
     apple_n[i][0]=(int)random(0, width);
     apple_n[i][1]=-90;
    }
  }

  //serial communication
  myserial = new Serial(this, "COM3", 9600);
  s_flag=0;
  s=1;
  
  //ざるの初期位置
  z_data = 800/2;
}

void draw() {
  if (myserial.available() >= 2)
  {
    high_byte = myserial.read();
    low_byte = myserial.read();
    r_data = (high_byte << 8) + low_byte;
    //z_data = width*r_data/1023;
    if(r_data >= 0 && r_data < 50){
      z_data -= 25;
    }else if(r_data >= 50 && r_data < 150){
      z_data -= 10;
    }else if(r_data >=150 && r_data < 450){
      z_data -= 5;
    }else if(r_data >=573 && r_data < 873){
      z_data += 5;
    }else if(r_data >=873 && r_data < 973){
      z_data += 10;
    }else if(r_data >=973 && r_data <= 1023){
      z_data += 25;
    }else{}

    if(z_data < 0) z_data=0;
    if(z_data > 800) z_data=800;

    myserial.write(s);
  }

  image(img0, width/2, height/2, width, height);

if(s_flag == 1){
if(miss != -1){
  //ざる表示
  image(img2, z_data, height-5, 70, 50);


 for(i=0; i<=1;i++){
  image(img1, apple_n[i][0], apple_n[i][1], 50, 50);

  
  if (apple_n[i][1] <= height) {
     /*
     switch (count/10){
      case 0: add=6; break;
      case 1: add=6; break;
      case 2: break;
      case 3: add=15; break;
      case 4: break;
      case 5: add=27; break;
      case 6: break;
      case 7: add=36; break;
      case 8: break;
      case 9: add=45; break;
      default: add=90;
     }
     */

     apple_n[i][1] += add;
    }
  else {
    apple_n[i][0] = (int)random(0, width);
    
    switch (i){
      case 0: apple_n[i][1] = 0; break;
      case 1: apple_n[i][1] = -90; break;
      //case 2: apple_n[i][1] = -90; break;
      //case 3: apple_n[i][1] = -90; break;
      //case 4: apple_n[i][1] = -20; break;
      //case 5: apple_n[i][1] = -18; break;
      //case 6: apple_n[i][1] = -12; break;
      //case 7: apple_n[i][1] = -10; break;
      //case 8: apple_n[i][1] = -6; break;
      //case 9: apple_n[i][1] = -3; break;
      default: apple_n[i][1] = -90;
    }
  }

  //if (apple_n[i][1]==height && apple_n[i][0] - mouseX < 10 && apple_n[i][0] - mouseX > -10) {
    if (apple_n[i][1]==height && apple_n[i][0] - z_data < 20 && apple_n[i][0] - z_data > -20) {
    textSize(30);
    fill(255, 0, 0);
    text("Get!", z_data, height-20);
    delay(3);
    count += 1;
    }
  //else if(apple_n[i][1]==height && (apple_n[i][0] - mouseX > 10 || apple_n[i][0] - mouseX < -10)) {
    else if(apple_n[i][1]==height && (apple_n[i][0] - z_data > 20 || apple_n[i][0] - z_data < -20)) {
    miss += 1;
  }
 }
}
else{
   fill(255, 0, 0);
   textSize(40);
   text("Game Over", 280, 100);

   //serial communication
   //s_flag=0;
   s=0;
   myserial.write(s);

  //twitter part start
  if(count >= 20){
    textSize(45);
    fill(255, 0, 0);
    text("Congratulations Hight Score!!", 60, 200);
    text("Points: "+ count, 280, 300);
    noLoop();
  }else{
    textSize(45);
    fill(255, 0, 0);
    text("Points: "+ count, 280, 300);
  }
  //twitter part end

}
  int textsize=40;
  fill(255, 255, 255);
  textSize(textsize);
  text("Apple: "+ count, 20, textsize);
  fill(255, 0, 0);
  text("Miss: "+ miss, 20, textsize*2);
}else{
  fill(255, 255, 255);
  textSize(36);
  text("Please click the mouse button to start.", 60, 430);
}

}

//serial communication
void mousePressed()
{
  myserial.write(s);
  s_flag = 1;
}